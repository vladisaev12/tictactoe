#!/usr/bin/env python3

import os
import sys
import tkinter as tk
from tkinter import messagebox
# import threading


table = []
player = 'X'
turn_count = 0
state = 'cont'


def init_table(size):
    table = []
    row = []
    for i in range(size):
        row.append(' ')
    for j in range(size):
        table.append(row.copy())

    return table


def display(size):
    row_delim = '+'
    for i in range(size):
        row_delim += '-+'
    print(row_delim)
    for row in table:
        print('|', end='')
        for field in row:
            print(field, end='|')
            sys.stdout.flush()
        print()
        print(row_delim)


def validate(size, x, y):
    if x < 0 or x > size - 1:
        print('Error: invalid row number')
        return False

    if y < 0 or y > size - 1:
        print('Error: invalid column number')
        return False

    return True


def flip_player():
    global player
    if player == 'X':
        player = 'O'
    else:
        player = 'X'


def turn(table, x, y):
    if not validate(len(table), x, y):
        return False

    if table[x][y] != ' ':
        print('Error: field is not empty')
        return False

    global player
    table[x][y] = player

    return True


def check_hv(table, player):
    size = len(table)
    for i in range(size):
        flag = True
        for j in range(size):
            flag &= table[i][j] == player
            if not flag:
                break
        if flag:
            return True

        flag = True
        for j in range(size):
            flag &= table[j][i] == player
            if not flag:
                break
        if flag:
            return True

    return False


def check_d(table, player):
    size = len(table)
    flag = True
    for i in range(size):
        flag &= table[i][i] == player
        if not flag:
            break
    if flag:
        return True

    flag = True
    for i in range(size):
        flag &= table[i][size - 1 - i] == player
        if not flag:
            break
    if flag:
        return True

    return False


def check(table, player, turn_count):
    if check_hv(table, player) or check_d(table, player):
        return player

    if turn_count == len(table)**2:
        return 'XO'

    return 'cont'


def on_clicked(button, x, y):
    print(f'on_clicked: x={x} y={y}')
    global table
    global state
    global turn_count

    if state != 'cont':
        return

    if turn(table, x, y):
        button['text'] = player
        turn_count += 1
        state = check(table, player, turn_count)
        flip_player()
#        display(size)

    if state == 'XO':
        print('Nobody win :(')
        messagebox.showinfo('The End', 'Nobody win :(')
        os._exit(0)
    elif state != 'cont':
        print(f'Player {state} win!')
        messagebox.showinfo('The End', f'Player {state} win!')
        os._exit(0)


def init_game(window, scale, button):
    size = scale.get()
    scale.destroy()
    button.destroy()

    global table
    table = init_table(size)

    for i in range(size):
        window.columnconfigure(i, weight=1, minsize=50)
        window.rowconfigure(i, weight=1, minsize=50)

        for j in range(size):
            button = tk.Button(master=window, text=" ")
            button['command'] = lambda button=button, i=i, j=j: (
                on_clicked(button, i, j)
            )
            button.grid(row=i, column=j, sticky='nswe')


def init_window():
    window = tk.Tk()
    window.title('tictactoe')
    window.attributes('-type', 'dialog')

    scale = tk.Scale(master=window, from_=3, to=10, orient=tk.HORIZONTAL)
    scale.pack()
    button = tk.Button(master=window, text="Start")
    button['command'] = lambda window=window, scale=scale, button=button: (
        init_game(window, scale, button)
    )
    button.pack()

    window.mainloop()


if __name__ == '__main__':
    init_window()

#    size = int(input('size of table = '))
#    table = init_table(size)
#    display(size)
#    init_window(size)
#    thr = threading.Thread(target=init_window,args=[size],daemon=True)
#    thr.start()
#    while state == 'cont':
#        print(f'Player {player}:')
#        x = int(input('row = '))
#        y = int(input('col = '))
#        if turn(table, x - 1, y - 1):
#            turn_count += 1
#            state = check(table, player, turn_count)
#            flip_player()
#        display(size)
#
#    if state == 'XO':
#        print('Nobody win :(')
#    else:
#        print(f'Player {state} win!')
#
    os._exit(0)
